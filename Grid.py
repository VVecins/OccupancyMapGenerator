from PIL import Image, ImageDraw
import numpy as np
import GridPoint
import Obstacles
import constants


class Grid:

    def __init__(self,
                 imageWidth,
                 imageHeight,
                 gridCols,
                 gridRows,
                 cellSize,
                 obstacles_max_percent_of_map):
        self.width = imageWidth
        self.height = imageHeight
        self.cols = gridCols
        self.rows = gridRows
        self.cellSize = cellSize
        self.obstacles_max_percent_of_map = obstacles_max_percent_of_map
        self.grid = np.zeros((self.cols, self.rows), dtype=object)
        self.stepSizeX = int(self.width / self.cols)
        self.stepSizeY = int(self.height / self.rows)
        self.image = Image.new(mode='RGB', size=(self.width, self.height), color=255)
        self.obstacles = 0

        for x in range(self.cols):
            for y in range(self.rows):
                self.grid[x][y] = GridPoint.GridPoint(x, self.stepSizeX, y, self.stepSizeY)

        # make borders
        for x in range(self.cols):
            self.grid[x][0].type = constants.MAP_GEN_BORDER
            self.grid[x][self.rows-1].type = constants.MAP_GEN_BORDER
        for y in range(self.rows):
            self.grid[0][y].type = constants.MAP_GEN_BORDER
            self.grid[self.cols-1][y].type = constants.MAP_GEN_BORDER

    def getPercentOfObstaclesInMap(self):
        arr = np.array(self.grid).ravel()
        return len([it for it in arr if it.type == constants.MAP_GEN_WALL]) * 100.0 / len(arr)

    def AddObstacles(self, count, obstacle_types=(constants.OBSTACLE_RECTANGLE,)):
        self.obstacles = np.zeros(count, dtype=object)
        i = 0
        is_completed = False
        while not is_completed or self.getPercentOfObstaclesInMap() >= self.obstacles_max_percent_of_map:
            color = constants.OBSTACLE_BLACK
            if self.getPercentOfObstaclesInMap() >= self.obstacles_max_percent_of_map:
                color = constants.OBSTACLE_WHITE

            # obstacle_type = obstacle_types[np.random.randint(0, len(obstacle_types))]

            # add to big maps for less circles
            a = np.random.randint(0, 3)
            if a == 2:
                obstacle_type = obstacle_types[0]
            else:
                obstacle_type = obstacle_types[1]

            self.obstacles[i] = Obstacles.Obstacle(self.cols, self.rows, obstacle_type)
            self.obstacles[i].ToGrid(self.grid, self.cols, self.rows, color)
            i += 1
            if i >= count:
                is_completed = True
                i = 0

    def GetAllPassages(self):
        passages = []
        for x in range(self.cols):
            for y in range(self.rows):
                if self.grid[x][y].type == constants.MAP_GEN_PASSAGE:
                    passages.append([x, y])
        return passages

    def GetRandomPassage(self):
        passages = self.GetAllPassages()
        return passages[np.random.randint(0, len(passages)-1)]

    def AddGoalPoint(self):
        self.goalPoint = self.GetRandomPassage()

    def GenMaze(self):
        start = self.GetRandomPassage()
        # make start point the same size as maze cell size
        start[0] = int(start[0] / self.cellSize)
        start[1] = int(start[1] / self.cellSize)
        # create new grid for larger passage size
        mazeCols = int(self.cols / self.cellSize)
        mazeRows = int(self.rows / self.cellSize)
        if self.cellSize != 1:
            mazeGrid = np.zeros((mazeCols, mazeRows), dtype=object)
            for x in range(mazeCols):
                for y in range(mazeRows):
                    mazeGrid[x][y] = GridPoint.GridPoint(x, int(self.stepSizeX / self.cellSize), y, int(self.stepSizeY / self.cellSize))
        else:
            mazeGrid = self.grid

        for x in range(mazeCols):
            for y in range(mazeRows):
                mazeGrid[x][y].type = constants.MAP_GEN_WALL

        frontierArr = []
        mazeGrid[start[0]][start[1]].type = constants.MAP_GEN_PASSAGE
        # add first frontier
        if start[0] + 2 < mazeCols - 1:
            frontierArr.append(mazeGrid[start[0] + 2][start[1]])
        if start[1] + 2 < mazeRows - 1:
            frontierArr.append(mazeGrid[start[0]][start[1] + 2])
        if start[0] - 2 > 1:
            frontierArr.append(mazeGrid[start[0] - 2][start[1]])
        if start[1] - 2 > 1:
            frontierArr.append(mazeGrid[start[0]][start[1] - 2])
        # Do while no free grid cells
        while frontierArr:
            neighbors = []
            cell = np.random.random_integers(0, len(frontierArr)-1)
            # check borders
            if frontierArr[cell].GetColNr() + 2 < mazeCols - 1:
                if mazeGrid[frontierArr[cell].GetColNr() + 2][frontierArr[cell].GetRowNr()].type == constants.MAP_GEN_PASSAGE:
                    neighbors.append(mazeGrid[frontierArr[cell].GetColNr() + 2][frontierArr[cell].GetRowNr()])
            if frontierArr[cell].GetRowNr() + 2 < mazeRows - 1:
                if mazeGrid[frontierArr[cell].GetColNr()][frontierArr[cell].GetRowNr() + 2].type == constants.MAP_GEN_PASSAGE:
                    neighbors.append(mazeGrid[frontierArr[cell].GetColNr()][frontierArr[cell].GetRowNr() + 2])
            if frontierArr[cell].GetColNr() - 2 > 0:
                if mazeGrid[frontierArr[cell].GetColNr() - 2][frontierArr[cell].GetRowNr()].type == constants.MAP_GEN_PASSAGE:
                    neighbors.append(mazeGrid[frontierArr[cell].GetColNr() - 2][frontierArr[cell].GetRowNr()])
            if frontierArr[cell].GetRowNr() - 2 > 0:
                if mazeGrid[frontierArr[cell].GetColNr()][frontierArr[cell].GetRowNr() - 2].type == constants.MAP_GEN_PASSAGE:
                    neighbors.append(mazeGrid[frontierArr[cell].GetColNr()][frontierArr[cell].GetRowNr() - 2])
            # connect cell to neighbor by setting value to 0
            if len(neighbors) == 1:
                neighbor = 0
            elif len(neighbors) == 0:
                break
            else:
                neighbor = np.random.random_integers(0, len(neighbors) - 1)

            mazeGrid[frontierArr[cell].GetColNr()][frontierArr[cell].GetRowNr()].type = constants.MAP_GEN_PASSAGE
            dx = int((frontierArr[cell].GetColNr() - neighbors[neighbor].GetColNr())/2)
            dy = int((frontierArr[cell].GetRowNr() - neighbors[neighbor].GetRowNr())/2)
            mazeGrid[frontierArr[cell].GetColNr() + dx][frontierArr[cell].GetRowNr() + dy].type = constants.MAP_GEN_PASSAGE
            # add new frontier cells
            if frontierArr[cell].GetColNr() + 2 < mazeCols - 1:
                if mazeGrid[frontierArr[cell].GetColNr() + 2][frontierArr[cell].GetRowNr()].type == constants.MAP_GEN_WALL:
                    frontierArr.append(mazeGrid[frontierArr[cell].GetColNr() + 2][frontierArr[cell].GetRowNr()])
            if frontierArr[cell].GetRowNr() + 2 < mazeRows - 1:
                if mazeGrid[frontierArr[cell].GetColNr()][frontierArr[cell].GetRowNr() + 2].type == constants.MAP_GEN_WALL:
                    frontierArr.append(mazeGrid[frontierArr[cell].GetColNr()][frontierArr[cell].GetRowNr() + 2])
            if frontierArr[cell].GetColNr() - 2 > 0:
                if mazeGrid[frontierArr[cell].GetColNr() - 2][frontierArr[cell].GetRowNr()].type == constants.MAP_GEN_WALL:
                    frontierArr.append(mazeGrid[frontierArr[cell].GetColNr() - 2][frontierArr[cell].GetRowNr()])
            if frontierArr[cell].GetRowNr() - 2 > 0:
                if mazeGrid[frontierArr[cell].GetColNr()][frontierArr[cell].GetRowNr() - 2].type == constants.MAP_GEN_WALL:
                    frontierArr.append(mazeGrid[frontierArr[cell].GetColNr()][frontierArr[cell].GetRowNr() - 2])
            del frontierArr[cell]
        # convert maze grid to original map grid
        if self.cellSize != 1:
            for x in range(mazeCols):
                for y in range(mazeRows):
                    for xi in range(self.cellSize):
                        for yi in range(self.cellSize):
                            self.grid[x * self.cellSize + xi][y * self.cellSize + yi].type = mazeGrid[x][y].type
        else:
            self.grid = mazeGrid
        del mazeGrid

        # regenerate border
        for x in range(self.cols):
            self.grid[x][0].type = constants.MAP_GEN_BORDER
            self.grid[x][self.rows-1].type = constants.MAP_GEN_BORDER
        for y in range(self.rows):
            self.grid[0][y].type = constants.MAP_GEN_BORDER
            self.grid[self.cols-1][y].type = constants.MAP_GEN_BORDER

    def Draw(self):
        # draw grid plane
        for x in range(self.cols):
            for y in range(self.rows):
                self.grid[x][y].DrawGridPoint(self.image)
        draw = ImageDraw.Draw(self.image)
        # draw goal point
        draw.rectangle(((self.goalPoint[0] * self.stepSizeX, self.goalPoint[1] * self.stepSizeY), (
            int((self.goalPoint[0] * self.stepSizeX) + self.stepSizeX),
            int((self.goalPoint[1] * self.stepSizeY) + self.stepSizeY))), fill="red", outline="red")

        del draw
        return self.image

    def fill_inaccessible_holes_dijkstra(self):
        # use value as one bit, true if smalles, false if inf. to improve work speed
        self.grid[self.goalPoint[0]][self.goalPoint[1]].value = True
        Open = []
        for x in range(self.cols):
            for y in range(self.rows):
                row_col = (x,y)
                Open.append(row_col)
        while Open:
            smallestVal = False
            smallest = 0
            for x in range(len(Open)):
                if self.grid[Open[x][0]][Open[x][1]].value > smallestVal:
                    smallest = self.grid[Open[x][0]][Open[x][1]]
                    index = x
                    break
            if smallest:
                if smallest.GetColNr() + 1 < self.cols - 1:
                    if self.grid[smallest.GetColNr() + 1][smallest.GetRowNr()].list == True:
                        if self.grid[smallest.GetColNr() + 1][smallest.GetRowNr()].type != constants.MAP_GEN_WALL:
                            self.grid[smallest.GetColNr() + 1][smallest.GetRowNr()].value = True
                if smallest.GetColNr() - 1 > 0:
                    if self.grid[smallest.GetColNr() - 1][smallest.GetRowNr()].list == True:
                        if self.grid[smallest.GetColNr() - 1][smallest.GetRowNr()].type != constants.MAP_GEN_WALL:
                            self.grid[smallest.GetColNr() - 1][smallest.GetRowNr()].value = True
                if smallest.GetRowNr() + 1 < self.rows - 1:
                    if self.grid[smallest.GetColNr()][smallest.GetRowNr() + 1].list == True:
                        if self.grid[smallest.GetColNr()][smallest.GetRowNr() + 1].type != constants.MAP_GEN_WALL:
                            self.grid[smallest.GetColNr()][smallest.GetRowNr() + 1].value = True
                if smallest.GetRowNr() - 1 > 0:
                    if self.grid[smallest.GetColNr()][smallest.GetRowNr() - 1].list == True:
                        if self.grid[smallest.GetColNr()][smallest.GetRowNr() - 1].type != constants.MAP_GEN_WALL:
                            self.grid[smallest.GetColNr()][smallest.GetRowNr() - 1].value = True

                self.grid[smallest.GetColNr()][smallest.GetRowNr()].list = False
                del Open[index]
            else:
                break
        for x in range(self.cols):
            for y in range(self.rows):
                if self.grid[x][y].value == False:
                    self.grid[x][y].type = constants.MAP_GEN_WALL
