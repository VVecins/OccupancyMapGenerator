import numpy as np
import constants

class Obstacle:
    def __init__(self, cols, rows, obstacle_type):
        self.shape = obstacle_type

        if obstacle_type == constants.OBSTACLE_CIRCLE:
            ratio = int(100 / constants.OBSTACLE_CIRCLE_PERCENT_OF_MAP)
            if cols < rows:
                self.radius = cols / ratio
            elif rows < cols:
                self.radius = rows / ratio
            else:
                self.radius = cols / ratio
            self.radius = np.random.randint(0, int(self.radius))
            self.center = (np.random.randint(0, cols), np.random.randint(0, rows))

        elif obstacle_type == constants.OBSTACLE_RECTANGLE:
            self.obstacleX = [0, 0]
            self.obstacleY = [0, 0]
            self.obstacleX[0] = np.random.random_integers(1, cols - 1)
            self.obstacleY[0] = np.random.random_integers(1, rows - 1)
            # random sign for object size
            j = np.random.random_integers(0, 1)
            if j < 1:
                j = -1
            self.obstacleX[1] = self.obstacleX[0] + j * np.random.random_integers(1, int(cols / 3))
            self.obstacleY[1] = self.obstacleY[0] + j * np.random.random_integers(1, int(cols / 3))
            if self.obstacleX[0] > self.obstacleX[1]:
                x = self.obstacleX[0]
                self.obstacleX[0] = self.obstacleX[1]
                self.obstacleX[1] = x
            if self.obstacleY[0] > self.obstacleY[1]:
                y = self.obstacleY[0]
                self.obstacleY[0] = self.obstacleY[1]
                self.obstacleY[1] = y

            self.obstacleX[0] = max(0, min(cols - 1, self.obstacleX[0]))
            self.obstacleX[1] = max(0, min(cols - 1, self.obstacleX[1]))
            self.obstacleY[0] = max(0, min(rows - 1, self.obstacleY[0]))
            self.obstacleY[1] = max(0, min(rows - 1, self.obstacleY[1]))

    def __invertCell(self, grid, x, y, color):
        if grid[x][y].type != "border":
            if color == "white":
                grid[x][y].type = "passage"
            elif color == "black":
                grid[x][y].type = "wall"

    def ToGrid(self, grid, map_width, map_height, color):
        if self.shape == constants.OBSTACLE_RECTANGLE:
            for x in range(self.obstacleX[0], self.obstacleX[1] + 1):
                for y in range(self.obstacleY[0], self.obstacleY[1] + 1):
                    self.__invertCell(grid, x, y, color)

        elif self.shape == constants.OBSTACLE_CIRCLE:
            for x in range(max(0, self.center[0] - self.radius), min(map_width, self.center[0] + self.radius + 1)):
                for y in range(max(0, self.center[1] - self.radius), min(map_height, self.center[1] + self.radius + 1)):
                    if (x - self.center[0]) ** 2 + (y - self.center[1]) ** 2 <= (self.radius ** 2 - constants.OBSTACLE_CIRCLE_EPSILON ** 2):
                        self.__invertCell(grid, x, y, color)

