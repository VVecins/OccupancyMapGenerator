
IMAGE_CELL_SIZE_PIXELS = 20
GRID_COLS = 32
GRID_ROWS = 32

OBSTACLE_COUNT_DEFAULT = 6
OBSTACLE_GENERATION_ITERATIONS = 3
OBSTACLE_WHITE = 'white'
OBSTACLE_BLACK = 'black'

OBSTACLE_MAX_PERCENT_OF_MAP = 50
OBSTACLE_CIRCLE_PERCENT_OF_MAP = 15
OBSTACLE_CIRCLE_EPSILON = 2.2
OBSTACLE_RECTANGLE = 'rectangle'
OBSTACLE_CIRCLE = 'circle'

MAP_TYPE_SHAPES = 'shapes'
MAP_TYPE_MAZE = 'maze'
MAP_TYPE_RANDOM = 'random'

MAP_GEN_PASSAGE = 'passage'
MAP_GEN_WALL = 'wall'
MAP_GEN_BORDER = 'border'

MAZE_CELL_SIZE = 1


MAP_TYPES = (MAP_TYPE_SHAPES, MAP_TYPE_MAZE)