import os
import time
import platform

PLATFORM_WINDOWS = 'Windows'

if platform.system() == PLATFORM_WINDOWS:
    import msvcrt
else:
    import fcntl


class FileUtils(object):

    @staticmethod
    def lock_file(f):
        while True:
            try:
                if platform.system() == PLATFORM_WINDOWS:
                    msvcrt.locking(f.fileno(), msvcrt.LK_NBLCK, 1)
                else:
                    fcntl.flock(f, fcntl.LOCK_EX | fcntl.LOCK_NB)
                break
            except IOError as e:
                time.sleep(0.1)

    @staticmethod
    def unlock_file(f):
        while True:
            try:
                if platform.system() == PLATFORM_WINDOWS:
                    msvcrt.locking(f.fileno(), msvcrt.LK_UNLCK, 1)
                else:
                    fcntl.flock(f, fcntl.LOCK_UN)
                break
            except IOError as e:
                time.sleep(0.1)

    @staticmethod
    def deleteDir(dirPath, is_delete_dir_path = False):
        if os.path.exists(dirPath):
            deleteFiles = []
            deleteDirs = []
            for root, dirs, files in os.walk(dirPath):
                for f in files:
                    deleteFiles.append(os.path.join(root, f))
                for d in dirs:
                    deleteDirs.append(os.path.join(root, d))
            for f in deleteFiles:
                os.remove(f)
            for d in deleteDirs:
                os.rmdir(d)
            if is_delete_dir_path:
                os.rmdir(dirPath)

    @staticmethod
    def createDir(dirPath):
        if not os.path.exists(dirPath):
            os.makedirs(dirPath)