import argparse
import os
import Grid
import numpy as np
import constants
from multiprocessing import Queue, Process
import time
import signal

from modules.file_utils import FileUtils

parser = argparse.ArgumentParser()
parser.add_argument("-map_cols", help="number of map cols and rows", default=constants.GRID_COLS, type=int)
parser.add_argument("-map_rows", help="number of map cols and rows", default=constants.GRID_ROWS, type=int)
parser.add_argument("-map_type", help="map type: 'maze', 'shapes'", default=constants.MAP_TYPE_RANDOM)

parser.add_argument("-obstacles_count", help="obstacle count", default=constants.OBSTACLE_COUNT_DEFAULT, type=int)
parser.add_argument("-obstacles_iterations", default=constants.OBSTACLE_GENERATION_ITERATIONS, type=int)
parser.add_argument("-obstacles_max_percent_of_map", default=constants.OBSTACLE_MAX_PERCENT_OF_MAP, type=int)

parser.add_argument("-output_show_map", default=False, type=lambda x: (str(x).lower() == 'true'))
parser.add_argument("-output_map_count", help="number of maps to generate", default=1, type=int)
parser.add_argument("-output_image_cell_size", help="in pixels", default=constants.IMAGE_CELL_SIZE_PIXELS, type=int)
parser.add_argument("-output_directory", help="directory to save map images", default="maps")

parser.add_argument("-maze_cell_size", help="change maze cell size, while not changing map size", default=constants.MAZE_CELL_SIZE, type=int)

parser.add_argument("-threads", default=8, type=int)

args = parser.parse_args()

obstacle_types = (constants.OBSTACLE_CIRCLE, constants.OBSTACLE_RECTANGLE)

def create_map(idx_map):
    current_map = None
    while current_map is None or current_map.getPercentOfObstaclesInMap() >= args.obstacles_max_percent_of_map:
        current_map = Grid.Grid(
            imageWidth=args.map_cols * args.output_image_cell_size,
            imageHeight=args.map_rows * args.output_image_cell_size,
            gridCols=args.map_cols,
            gridRows=args.map_rows,
            cellSize=args.maze_cell_size,
            obstacles_max_percent_of_map=args.obstacles_max_percent_of_map * 0.7)

        map_type = args.map_type
        if map_type == constants.MAP_TYPE_RANDOM:
            map_type = constants.MAP_TYPES[np.random.randint(0, len(constants.MAP_TYPES))]

        if map_type == constants.MAP_TYPE_MAZE:
            current_map.GenMaze()

        for _ in range(args.obstacles_iterations):
            current_map.AddObstacles(args.obstacles_count, obstacle_types)

        current_map.AddGoalPoint()
        current_map.fill_inaccessible_holes_dijkstra()

    print(f'generating: {idx_map+1} of {args.output_map_count}')
    print(f'obstacles in map {current_map.getPercentOfObstaclesInMap()}%')

    current_image = current_map.Draw()
    imageName = "%s/map%04d.png" % (args.output_directory, idx_map)
    current_image.save(imageName)
    if args.output_show_map:
        current_image.show()
    del current_image
    del current_map


def worker(queue):
    while not queue.empty():
        idx = queue.get()
        create_map(idx)


if __name__ == '__main__':

    if (args.map_cols) % args.maze_cell_size != 0 or (args.map_rows) % args.maze_cell_size != 0:
        print(f'map dimensions and maze_cell_size: {args.maze_cell_size} division not integer')
        quit()

    FileUtils.createDir(args.output_directory)
    FileUtils.deleteDir(args.output_directory, is_delete_dir_path=False)

    queue = Queue()
    for i in range(args.output_map_count):
        queue.put(i)

    processes = []
    for _ in range(args.threads):
        process = Process(target=worker, args=(queue,))
        process.daemon = True
        process.start()
        processes.append(process)

    start = time.time()

    for process in processes:
        process.join()

    print('Entire job took:', time.time() - start)